Play Random Episode
===================

Introduction
------------
**Play Random Episode is a Chrome extension that adds a button to the Netflix interface to randomly choose an episode in a series.**

Get the extenstion at the Chrome Store:
https://chrome.google.com/webstore/detail/ggclanokennhoaeldbffpmnfakhdbmmg

Finally, you can easily pick a random episode on Netflix with the click of a button. This extension places a new Play Random Episode button on movie details pages, but only when there are multiple episodes.

**This is not an official Netflix product.**

Usage
-----
After you install the extension, just head to Netflix.com to find the Play Random Episode button on the description page for your favorite series.

Hovering your mouse over the button will show the selected random episode. To choose a different episode, simply move the mouse off the button and hover over the button again.

How it Works
------------
The Play Random Episode extension is really simple; it loads the list of episodes for each season using Javascript (more specifically, the jQuery 1.7.2 library) and chooses one at random. It only works with episodic movies on the Netflix Watch Instantly website.

Future Features
---------------
- Add the button to more locations in the Netflix interface.
- Create a new function to play a random episode in a random series from a particular queue.
- Create a new "shuffle mode", where episodes in a series are played randomly in a playlist without repeating a previous choice.

Have better ideas? Send them my way!
ithackston@gmail.com

