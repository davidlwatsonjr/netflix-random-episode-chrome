var seasonsData = [],
    loadingDone = false;

var randomBtn = $('<span id="randomBtn" class="btnWrap mltBtn mltBtn-s60"><a href="#" class="btn btn-60 btn-ED-60"><span class="inr">Random Episode</span></a></span>');

var randomBOB = $('<div class="episodeBOB leftBOB densityEpisodeBOB" style="display: none; visibility: visible; top: -65px; left: -330px;" />')
                    .append('<span class="ebob-arrow transp nse" style="top: 52px; height:52px; left:251px;" />')
                    .append('<div class="ebob-head transp" />')
                    .append('<div class="ebob-content transp" />')
                    .append('<div class="ebob-foot transp" />');

var throbber = $('<div class="randomLoading" />')
                    .css('background-image','url(' + chrome.extension.getURL("images/throbber.gif") + ')');

var loadingScr = $('<div class="ebob-content transp" />')
                    .append('<h3 class="heading"> Loading Episodes </h3>')
                    .append(throbber);
                    
var loadingBOB = $('<div />')
                    .append(loadingScr)
                    .append('<a href="#" />');

getRandomEpisode = function() {
    if(loadingDone) {
        var ep = seasonsData[Math.floor(Math.random() * seasonsData.length)];
        replaceBob($(ep));
    }
}

loadEpisodes = function(seriesHref) {
    var seasons = null;
    
    if(seriesHref) {
        // series href given
        $.get(seriesHref, function (data) {
            seasons = $(data).find("#seasonsNav .seasonItem a");
        });
    } else {
        // assume the current window location is the series page
        seriesHref = window.location.href.replace("#","");
        seasons = $("#seasonsNav .seasonItem a");
    }
    
    // show loading screen
    replaceBob(loadingBOB);
    
    if(seasons.length) {
        // multiple seasons listed, load each tab and get the episodes
        var numLoaded = 0;
        
        seasons.each(function(i,e) {
            $.getJSON(seriesHref + "&actionMethod=seasonDetails&seasonId=" + 
                      $(e).attr("data-vid") + "&seasonKind=ELECTRONIC", 
                      function(data,textStatus) {
                numLoaded += 1;
                
                $(data.html).find(".episodeList li").each(function(i,e) {
                    seasonsData.push(e);
                });
                
                if(numLoaded == seasons.length) {
                    loadingDone = true;
                    getRandomEpisode();
                }
            });
        });
    } else {
        // just one season, no need to load more tabs
        $("#seasonDetail").find(".episodeList li").each(function(i,e) {
            seasonsData.push(e);
        });
        
        loadingDone = true;
        getRandomEpisode();
    }
}

replaceBob = function(newBob) {
    randomBOB.find('.ebob-content').html('').append(newBob.find('.ebob-content').html());
    randomBtn.find('a.btn').attr('href',newBob.find('a').attr('href'));
}

isEpisodicDisplayPage = function() {
    return $("#displaypage-content").find(".episodeList").length > 0;
}

$(function(){
    if(isEpisodicDisplayPage()) {
        // append bob and button to bodyTop
        $("#displaypage-overview-details").find(".actions").append(randomBtn.append(randomBOB.hide()));
        randomBtn.find(".btn").css("background-image",'url(' + chrome.extension.getURL("images/random.png") + ')');
        
        // set up button hover
        randomBtn.hover(function(e) {
            getRandomEpisode();
            randomBOB.show();
        }, function(e) {
            randomBOB.hide();
        });
        
        // load episodes
        loadEpisodes();
    }
});
